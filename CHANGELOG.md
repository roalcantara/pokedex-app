## [1.0.1](https://github.com/roalcantara/pokedex-app/compare/v1.0.0...v1.0.1) (2022-06-13)


### Bug Fixes

* **ci:** Add missing env vars ([b03cd7c](https://github.com/roalcantara/pokedex-app/commit/b03cd7c2c59ee2b61f6e7ef56b8268d8f5ab2c86))

# 1.0.0 (2022-06-13)


### Bug Fixes

* **deps:** Bump dependencies to latest ([ffd4b7e](https://github.com/roalcantara/pokedex-app/commit/ffd4b7ec5798b9004d28a72c26eae35ca602fa75))
