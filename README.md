# Pokedex App

A Simple Android Kotlin App

[![Check](https://github.com/roalcantara/pokedex-app/actions/workflows/check.yml/badge.svg)](https://github.com/roalcantara/pokedex-app/actions/workflows/check.yml)
[![Release](https://github.com/roalcantara/pokedex-app/actions/workflows/release.yml/badge.svg)](https://github.com/roalcantara/pokedex-app/actions/workflows/release.yml)

[![MIT license](https://img.shields.io/badge/License-MIT-brightgreen.svg?style=flat-square)](LICENSE)
[![Contributor Covenant](https://img.shields.io/badge/Contributor%20Covenant-2.0-4baaaa.svg?style=flat-square)][2]
[![standard-readme compliant](https://img.shields.io/badge/readme%20style-standard-brightgreen.svg?style=flat-square)][4]
[![Editor Config](https://img.shields.io/badge/Editor%20Config-1.0.1-crimson.svg?style=flat-square&logo=editorconfig)][3]
[![Make](https://img.shields.io/badge/make-4.3-green.svg?style=flat-square)][6]
[![Conventional Commits](https://img.shields.io/badge/Conventional%20Commits-1.0.0-yellow.svg)][8]
[![Gradle plugin](https://img.shields.io/badge/plugins.gradle.org-com.diffplug.spotless-blue.svg)][12]
[![Semantic Release](https://img.shields.io/badge/%20%20%F0%9F%93%A6%F0%9F%9A%80-semantic--release-e10079.svg)][13]

## Install

`git clone https://github.com/roalcantara/pokedex-app`

### Dependencies

- [git][5]
- [make][6]
- [Gitlint][7]
- [Precommit][8]
- [ASDF][10]
- [Direnv][11]

## Usage

- Shows the help screen

  ```sh
  make help
  ```

- Cleans up the app

  ```sh
  make clear
  ```

- Delete global cache directory

  ```sh
  make clear_cache
  ```

- Cleans up the app and the cache

  ```sh
  make clear_all
  ```

- Build the application

  ```sh
  make build
  ```

- Build project complete from scratch

  ```sh
  make rebuild
  ```

- Assemble the application

  ```sh
  make assemble
  ```

- Run the application

  ```sh
  make run
  ```

- Run the application

  ```sh
  make start
  ```

- Publish a Build Scan

  ```sh
  make scan
  ```

- Run all of the unit tests in the project

  ```sh
  make tests
  ```

- Run the instrumentation tests

  ```sh
  make instrumentation
  ```

- Build and start your instrumented tests on your Android device

  ```sh
  make e2e
  ```

- Stop all Jobs

  ```sh
  make stop
  ```

- Reset the [dependencies](https//is.gd/6GHdhj)

  ```sh
  make reset
  ```

- Runs the linters

  ```sh
  make lint
  ```

- Fix lint violations

  ```sh
  make lint_fix
  ```

- Aggregates all verification tasks: code style, library vulnerability scans, tests, and integration tests

  ```sh
  make check
  ```

## Acknowledgements

- [Standard Readme][4]
- [Conventional Commits][9]
- [Semantic Release][12]

## Contributing

- Bug reports and pull requests are welcome on [GitHub][0]
- Do follow [Editor Config][3] rules.
- Do follow [Gitlint][7] rules.
- Do follow [Conventional Commits][9] rules.
- Everyone interacting in the project is expected to follow the [Contributor Covenant][2] code of conduct.

## License

The project is available as open source under the terms of the [MIT][1] [License](LICENSE)

[0]: https://github.com/roalcantara/pokedex-app
[1]: https://opensource.org/licenses/MIT "Open Source Initiative"
[2]: https://contributor-covenant.org "A Code of Conduct for Open Source Communities"
[3]: https://editorconfig.org "EditorConfig"
[4]: https://github.com/RichardLitt/standard-readme "Standard Readme"
[5]: https://git-scm.com "Git"
[6]: https://www.gnu.org/software/make/manual/make.html "GNU Make"
[7]: https://jorisroovers.com/gitlint "GitLint: git commit message linter"
[8]: https://pre-commit.com "A framework for managing and maintaining multi-language pre-commit hooks"
[9]: https://conventionalcommits.org "Conventional Commits"
[10]: https://asdf-vm.com "ASDF: Manage multiple runtime versions with a single CLI tool"
[11]: https://direnv.net "Direnv: Unclutter your .profile"
[12]: https://plugins.gradle.org/plugin/com.diffplug.spotless "Spotless: A tool for automatically running your code through a linter"
[13]: https://semantic-release.gitbook.io/semantic-release "Semantic Release"
